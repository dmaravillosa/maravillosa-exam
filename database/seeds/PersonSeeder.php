<?php

use Illuminate\Database\Seeder;

class PersonSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Person::create(['first_name' => 'John', 'last_name' => 'Doe', 'contact_number' => '09123456789']);
        \App\Person::create(['first_name' => 'Jane', 'last_name' => 'Williams', 'contact_number' => '09987654321']);
        \App\Person::create(['first_name' => 'Jack', 'last_name' => 'James', 'contact_number' => '09543216789']);
    }
}
